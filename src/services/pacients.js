import firebase from './firebase-config';

const patientsRef = firebase.database().ref('pacients');

function createPatient(patient){
    const body = {...patient};
    const key = body.key;
    delete body.key;

    if (!key) return patientsRef.push(body);
    else return patientsRef.child(key).update(body);
}

async function getPatients(){
    let patients = [];
    
    await patientsRef.once('value', (snapshot) => {
        
        snapshot.forEach((childSnapshot) => {
            const key = childSnapshot.key;
            const data = childSnapshot.val();

            patients.push({key, ...data});            
        });        
    });
    
    return patients;
}

async function getPatient(key){
    let patient;
    await patientsRef.child(key).once('value', (item) => {        
        patient = {key: item.key, ...item.val()};
    });

    return patient;
}

export {
    createPatient,
    getPatients,
    getPatient
};