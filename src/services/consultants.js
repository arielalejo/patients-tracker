import firebase from './firebase-config';

const contultationsRef = firebase.database().ref('contultations');

export async function getContultations() {    
    let contultations = [];
    
    await contultationsRef.once('value', (snapshot) => {
        
        snapshot.forEach((childSnapshot) => {
            const key = childSnapshot.key;
            const data = childSnapshot.val();

            contultations.push({key, ...data});            
        });        
    });
    
    return contultations;

    
}