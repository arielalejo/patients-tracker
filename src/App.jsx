import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import './App.css';
import Home from './components/home/Home';
import Form from './components/form/Form';
import PacientDetails from './components/pacient/PacientDetails';

export default function App() {
    return (
        <>
            <Navbar/>  
            <div className="app-body">
                <Switch>
                    <Route path='/home' component={Home}/>
                    <Route path='/form/:key' component={Form}/>
                    <Route path='/form' component={Form}/>
                    <Route path='/pacients' component={PacientDetails}/>
                    <Redirect from='/' exact to='/home'/>
                    <Redirect to='/'/>
                </Switch>
            </div>
        </>
    );
    
}
