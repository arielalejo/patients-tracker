import Joi from 'joi-browser';

const schema = {
    key : Joi.string().optional().allow(''),
    patientCode: Joi.string().required().label('Codigo de Paciente'),
    treatement: Joi.object().keys({
        description: Joi.string().required().label('Descripcion')
    }),
    history: Joi.object().keys({
        antecedents: Joi.string().optional().allow(''),
        clinicalPicture: Joi.string().optional().allow(''),
        evolution: Joi.string().optional().allow('')
    }),
    examns: Joi.object().optional(),
    contultations: Joi.array().optional()
    
};

export function validateForm (data){
    const options = { abortEarly: false };
    const {error} = Joi.object(schema).validate(data, options);

    if (!error) return null;
    
    const errors = {};
    error.details.forEach(err => errors[err.path[0]] = err.message);
    return errors;
}