import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    buttons: {
        maxWidth: '80%',
        marginLeft:'10%',
        marginTop: '40px',
    },
    button:{
        marginRight:'30px'
    },
    formContainer:{
        display: 'flex',
        justifyContent: 'center',
    },
    formContent:{
        maxWidth: '80%',                
    }
}));
