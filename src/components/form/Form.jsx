import React, { useEffect, useState } from 'react';
import Input from '../common/Input';
import {
    Grid,
    Typography,
    Button,
    FormControlLabel,
    Checkbox,
    FormLabel,
    FormGroup,
    Snackbar,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { Link } from 'react-router-dom';
import { getContultations } from '../../services/consultants';
import { createPatient, getPatient } from '../../services/pacients';
import { useStyles } from './styles';
import { validateForm } from './validations';
import SelectNative  from '../common/SelectNative';
import SelectMui from '../common/SelectMui';

const initialHistory = { antecedents: '', clinicalPicture: '', evolution: '' };
const initialExamns = {
    laboratory: { complete: false },
    image: { complete: false },
};

export default function Form(props) {
    const classes = useStyles();
    const [key, setKey] = useState('');
    const [patientCode, setPatientCode] = useState('');
    const [history, setHistory] = useState(initialHistory);
    const [treatement, setTratament] = useState({ description: '' });
    const [examns, setExamns] = useState(initialExamns);
    const [patientContultations, setPatientContultations] = useState([]);
    const [contultationsList, setContultationsList] = useState([]);
    const [selectedContultation, setSelectedContultation] = useState('');
    const [errors, setErrors] = useState({});
    const [openSnack, setOpenSnack] = useState(false);

    const handleExamnsChange = (event) => {
        setExamns({
            ...examns,
            [event.target.name]: { complete: event.target.checked },
        });
    };

    const handleToogleContultation = (event, checkedContultation) => {
        const cloneContults = [...patientContultations];
        const index = cloneContults.findIndex(
            (c) => c.contultationKey == checkedContultation.contultationKey
        );

        cloneContults[index].complete = event.target.checked;
        setPatientContultations(cloneContults);
    };

    const handleSelectContultaton = (event) => {
        const selectedContultation = event.target.value;
        setSelectedContultation(selectedContultation);

        if(!selectedContultation) return;

        const contultation = patientContultations.find(
            (c) => c.contultationKey === selectedContultation
        );

        if (contultation) return;
        setPatientContultations([
            ...patientContultations,
            { contultationKey: selectedContultation, complete: false },
        ]);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') return;

        setOpenSnack(false);
    };

    const fetchContultations = async () => {
        const fetchedContultations = await getContultations();
        setContultationsList(fetchedContultations);
    };

    const fetchCurrentPatient = async () => {
        const { key } = props.match.params;
        if (key === 'new') return;

        const patient = await getPatient(key);

        setKey(patient.key);
        setPatientCode(patient.patientCode);
        setTratament(patient.treatement);
        setHistory(patient.history);
        setExamns(patient.examns);
        setPatientContultations(
            patient.contultations ? patient.contultations : []
        );
        setSelectedContultation(
            patient.contultations
                ? patient.contultations[0].contultationKey
                : ''
        );
    };

    useEffect(() => {
        fetchContultations();
        fetchCurrentPatient();
    }, []);

    const handleSubmit = async () => {
        const patient = {
            key,
            patientCode,
            history,
            treatement,
            examns,
            contultations: patientContultations,
        };

        const errors = validateForm(patient);
        if (errors) {
            //setErrors(errors? errors : {});
            setOpenSnack(true);
            return;
        }
        await createPatient(patient);
        props.history.replace('/home');
    };

    const getContultationLabel = (patientContult) => {
        const cloneContultList = [...contultationsList];
        const index = cloneContultList.findIndex(
            (contultItem) => contultItem.key == patientContult.contultationKey
        );
        return cloneContultList[index].name;
    };

    return (
        <>
            <Typography variant="h4">Formulario del Paciente</Typography>
            <br />
            <br />
            <form>
                <div className={classes.formContainer}>
                    <Grid container spacing={3} className={classes.formContent}>
                        <Input
                            label="Codigo Paciente"
                            value={patientCode}
                            name="patientCode"
                            onChange={(e) => setPatientCode(e.target.value)}
                            required
                            //error={errors['patientCode'] ? true : false}
                        />
                        <Input
                            label="Tratamiento"
                            value={treatement.description}
                            name="treatement"
                            onChange={(e) => setTratament({ description: e.target.value })}
                            required
                            //error={errors['treatement'] ? true : false}
                        />

                        <Input
                            label="Antecedentes"
                            value={history.antecedents}
                            name="antecedents"
                            onChange={(e) =>
                                setHistory({
                                    ...history,
                                    antecedents: e.target.value,
                                })
                            }
                        />
                        <Input
                            label="Cuadro Clinico"
                            value={history.clinicalPicture}
                            name="clinicalPicture"
                            onChange={(e) =>
                                setHistory({
                                    ...history,
                                    clinicalPicture: e.target.value,
                                })
                            }
                        />
                        <Input
                            label="Evolucion"
                            value={history.evolution}
                            name="evolution"
                            onChange={(e) =>
                                setHistory({
                                    ...history,
                                    evolution: e.target.value,
                                })
                            }
                        />
                        <Grid item xs={12} sm={6}>
                            <FormLabel component="legend">Examenes</FormLabel>
                            <FormGroup>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={examns.laboratory.complete}
                                            onChange={handleExamnsChange}
                                            name="laboratory"
                                        />
                                    }
                                    label="Laboratorio"
                                />
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={examns.image.complete}
                                            onChange={handleExamnsChange}
                                            name="image"
                                        />
                                    }
                                    label="Imagen"
                                />
                            </FormGroup>
                        </Grid>
                        <SelectMui
                            label={'Lista de Interconsultas'}
                            componentName={'contultationmenu'}
                            value={selectedContultation}
                            items={contultationsList}
                            onOptionChange={handleSelectContultaton}
                        />                       
                        <Grid item xs={12} sm={6}></Grid>
                        <Grid item xs={12} sm={6}>
                            {patientContultations.length > 0 && (
                                <FormLabel component="legend">
                                Consultas del Paciente
                                </FormLabel>)}
                            <FormGroup>
                                {patientContultations.map((contult) => (
                                    <FormControlLabel
                                        key={contult.contultationKey}
                                        control={
                                            <Checkbox
                                                checked={contult.complete}
                                                onChange={(e) =>
                                                    handleToogleContultation(
                                                        e,
                                                        contult
                                                    )
                                                }
                                                name="contultation"
                                            />
                                        }
                                        label={getContultationLabel(contult)}
                                    />
                                ))}
                            </FormGroup>
                        </Grid>
                    </Grid>
                </div>

                <div className={classes.buttons}>
                    <Button
                        className={classes.button}
                        component={Link}
                        to="/home"
                        variant="contained"
                        color="default"
                    >
                        Regresar
                    </Button>
                    <Button
                        className={classes.button}
                        variant="contained"
                        color="primary"
                        type="button"
                        onClick={handleSubmit}
                    >
                        Guardar
                    </Button>
                </div>
            </form>
            <Snackbar
                anchorOrigin={{ vertical: 'top', horizontal: 'right', }}
                open={openSnack}
                autoHideDuration={3000}
                onClose={handleClose}                
            >
                <MuiAlert onClose={handleClose} elevation={6} variant="filled" severity="error">
                    Codigo Paciente y Tratamiento es Requerido
                </MuiAlert>
            </Snackbar>
        </>
    );
}
