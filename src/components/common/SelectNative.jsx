import React from 'react';
import { InputLabel, Grid, NativeSelect } from '@material-ui/core';

export default function SelectNative({label, componentName, value, items, onOptionChange }) {
    return (
        <Grid item xs={12} sm={6}>
            <InputLabel htmlFor='{componentName}'>
                {label}
            </InputLabel>
            <NativeSelect
                style={{ minWidth: '220px' }}
                value={value}
                onChange={(e) => onOptionChange(e)}
                inputProps={{
                    name: componentName,
                    id: {componentName},
                }}
            >
                <option></option>
                {items.map((item) => (
                    <option value={item.key} key={item.key}>
                        {item.name}
                    </option>
                ))}
            </NativeSelect>
        </Grid>
    );
}
