import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';

export default function SelectMui({
    label,
    componentName,
    value,
    items,
    onOptionChange,
}) {
    return (
        <Grid item xs={12} sm={6}>
            <InputLabel id={componentName}>
                {label}
            </InputLabel>
            <Select
                style={{ minWidth: '220px' }}
                labelId={componentName}
                id={componentName}
                value={value}
                onChange={e => onOptionChange(e)}
            >
                <MenuItem value=""><em> Ninguna </em></MenuItem>
                {items.map((item) => (
                    <MenuItem key={item.key} value={item.key}>
                        {item.name}
                    </MenuItem>
                ))}
            </Select>
        </Grid>
    );
}
