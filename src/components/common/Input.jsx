import React from 'react';
import {Grid, TextField} from '@material-ui/core';

export default function Input({label,name, value, ...args}) {

    return (
        <Grid item xs={12} sm={6}>
            <TextField
                id="filled-required"   
                name={name}                
                label={label}
                value={value}
                {...args}
            />
        </Grid>
    );
}
