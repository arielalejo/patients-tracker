import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import * as patientService from '../../services/pacients';
import {useStyles} from './styles';


export default function Home() {
    const classes = useStyles();
    const [patients, setPatients] = useState([]);

    const fetchPatients = async () => {
        const data = await patientService.getPatients();
        setPatients(data);
    };

    useEffect(()=>{
        fetchPatients();
    }, []);

    const isContultationsComplete = (patient) => {        
        const {image, laboratory} = patient.examns;
        const contultations = patient.contultations ? patient.contultations : [];
        
        let isContulComplete = true && image.complete && laboratory.complete;
        contultations.forEach(contult =>  {
            isContulComplete &= contult.complete ;
        });
        
        return isContulComplete;
    };

    return (
        <div>
            <Button variant="contained" color="primary" component={Link} to='/form/new'>
                Crear Paciente
            </Button>   
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Código del Paciente</TableCell>
                            <TableCell align="right">Tratamiento</TableCell>
                            <TableCell align="right">Antecedentes</TableCell>
                            <TableCell align="right">Cuadro Clinico</TableCell>
                            <TableCell align="right">Evolución</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {patients.map((patient) => (
                            <TableRow key={patient.key} className={!isContultationsComplete(patient)? classes.warning: ''}>
                                <TableCell component="th" scope="row">
                                    <Link to={'/form/' + patient.key}> {patient.patientCode}</Link>
                                </TableCell>
                                <TableCell align="right">{patient.treatement.description}</TableCell>
                                <TableCell align="right">{patient.history.antecedents}</TableCell>
                                <TableCell align="right">{patient.history.clinicalPicture}</TableCell>
                                <TableCell align="right">{patient.history.evolution}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>         
        </div>
    );
}
