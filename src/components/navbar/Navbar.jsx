import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import {useStyles} from './styles';
import logo from '../../assets/logo.png';
import { Link } from 'react-router-dom';

export default function Navbar() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton component={Link} to="/" edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <img src={logo} alt="patients-tracker" height="35px" style={{borderRadius: '50%'}}/>
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                            Patients Tracker (beta)                        
                    </Typography>                                    

                </Toolbar>
            </AppBar>
        </div>
    );
}
