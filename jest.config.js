module.exports = {
    roots: ['<rootDir>/tests'],
    // transform: {
    //     '^.+\\.tsx?$': 'ts-jest',
    //     '.+\\.(css|styl|less|sass|scss)$': 'jest-transform-css'
    // },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.jsx?$',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    setupFiles: [
        './src/setupTests.js',
        './setEnvVars.js'
    ],
    verbose: true,
    moduleNameMapper: {
        '.+\\.(css|styl|less|sass|scss|png|jpg|jpeg|ttf|woff|woff2)$': 'identity-obj-proxy',
        'fontsource-roboto': 'identity-obj-proxy'
    }
};